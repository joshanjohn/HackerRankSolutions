﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.DataStructures._02_Trees
{
    class BinaryTreeHeight
    {
        #region Program Description
        /*
         * 
         * The height of a binary tree is the number of nodes on the largest path from 
         * root to any leaf. You are given a pointer to the root of a binary tree. Return 
         * the height of the tree. You only have to complete the function.
         * 
         * Input Format
         * 
         * You are given a function,

            int height_of_bt(node * root)
            {

            }
            Output Format

            Return a single value equal to the height of the binary tree.

            Sample Input

                 3
               /   \
              5     2
             / \    /
            1   4  6
                  /
                 7
         
            Sample Output
            ===================
            4
         
            Explanation
            ===================
            The maximum length root to leaf path is 3->2->6->7. There are 4 nodes in this 
            path. Therefore the height of the binary tree = 4.
         
         */
        #endregion

        /*
        class Node {
           int data;
           Node left;
           Node right;
        }
       */

        #region Java Recursive Solution
        /*
            int height(Node root)
            {
               if (root == null)
                   return 0;
       
               int leftHeight = height(root.left);
               int rightHeight = height(root.right);
       
               if (leftHeight > rightHeight)
                   return leftHeight+1;
               else
                   return rightHeight+1;
            }
         */
        #endregion

        #region Java Iterative Solution
        /*
           int height(Node root)
           {
               if (root == null)
                   return 0;

               int height = 0;
               Queue<Node> processQ = new LinkedList<Node>();
               processQ.add(root);

               while(!processQ.isEmpty()){
           
                   ++height;
           
                   int itemCount = processQ.size();
           
                   while(itemCount > 0){
                       Node current = processQ.remove();
           
                       if (current.left != null){
                           processQ.add(current.left);
                       }

                       if (current.right != null){
                           processQ.add(current.right);
                       }
                       --itemCount;
                   }
               }

               return height;
           }
         */
        #endregion

    }
}
