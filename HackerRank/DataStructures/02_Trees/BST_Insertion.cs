﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.DataStructures._02_Trees
{
    class BST_Insertion
    {
        #region Program Description
        /*
            Problem Statement
            ====================
            You are given a pointer to the root of a binary search tree and a 
            value to be inserted into the tree. Insert this value into its 
            appropriate position in the binary search tree and return the root 
            of the updated binary tree. You just have to complete the function.

            Input Format
            ====================
            You are given a function,

            node * insert (node * root ,int value)
            {

            }
         
            node is defined as :

            struct node
            {
            int data;
            node * left;
            node * right;
            }node;
         
            Output Format
            ====================
            Return the root of the binary search tree after inserting the value 
            into the tree.

            Sample Input
            ====================
         
                    4
                   / \
                  2   7
                 / \
                1   3
            The value to be inserted is 6.

            Sample Output
            ====================
         
                     4
                   /   \
                  2     7
                 / \   /
                1   3 6
         
         */
        #endregion

        #region Java Program - Iterative Solution
        /*
            static Node Insert(Node root, int data)
            {
                Node n = new Node();
                n.data = data;

                if (root == null)
                    return n;

                Node pointer = root;

                while (pointer.left != null || pointer.right != null)
                {
                    if (data >= pointer.data && pointer.right != null)       // right of tree node
                    { 
                        pointer = pointer.right;
                        continue;
                    }
                    else if (data < pointer.data && pointer.left != null)    // left of tree node
                    { 
                        pointer = pointer.left;
                        continue;
                    }

                    if (pointer.right == null || pointer.left == null)
                        break;
                }

                // Once both left and right tree are nulls
                if (data >= pointer.data)
                    pointer.right = n;          // right of tree node
                else
                    pointer.left = n;           // left of tree node

                return root;
            }
         */
        #endregion
    }
}
