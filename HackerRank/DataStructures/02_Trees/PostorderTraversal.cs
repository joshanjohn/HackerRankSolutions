﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.DataStructures._02_Trees
{
    class PostorderTraversal
    {
        // Left - Right - Center

        #region Program Description
        /*
         
            You are given a pointer to the root of a binary tree; print the values in post-order traversal.

            You only have to complete the function.

            Input Format 
            You are given a function,

            void Postorder(node *root) {

            }
            Output Format 
            Print the values on a single line separated by space.

            Sample Input

                 3
               /   \
              5     2
             / \    /
            1   4  6
            Sample Output

            1 4 5 6 2 3
         
         */
        #endregion

        #region Java Program
        /* you only have to complete the function given below.  
            Node is defined as  

            class Node {
                int data;
                Node left;
                Node right;
            }

            */
            //void Postorder(Node root) {
            //    if (root != null){
            //        Postorder(root.left);
            //        Postorder(root.right);
            //        System.out.print(root.data);
            //        System.out.print(" ");
            //    }
            //}
        #endregion
    }
}
