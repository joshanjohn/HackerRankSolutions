﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.DataStructures._01_LinkedLists
{
    class MergePointOfTwoLists
    {
        #region Program Description
        /*
         * You’re given the pointer to the head nodes of two linked lists that merge together at some node. Find the node at which this merger happens. The two head nodes will be different and neither will be NULL.
         * 
         * [List #1] a--->b--->c
                                 \
                                  x--->y--->z--->NULL
                                 /
                 [List #2] p--->q
         * 
         * In the above figure, both list merges at node x.
         * 
         * Input Format 
         * ==============
         * You have to complete the int FindMergeNode(Node* headA, Node* headB) method which takes two arguments - 
         * the heads of the linked lists. You should NOT read any input from stdin/console.
         * 
         * Output Format 
         * ==============
         * Find the node at which both lists merge and return the data of that node. Do NOT print anything to 
         * stdout/console.
         * 
         * Sample Input
         * ==============
         * 
         * 1
         *  \
         *   2--->3--->NULL
         *  /
         * 1
         * 
         * 1--->2
         *       \
         *        3--->Null
         *       /
         *      1
         *      
         * Sample Output
         * ==============
         * 
         * 2
         * 3
         * 
         * Explanation
         * ==============
         * 1. As shown in the Input, 2 is the merge point. 
         * 2. Similarly 3 is merging point
         * 
         */
        #endregion

        #region Java Program
        /*
         
        int FindMergeNode(Node headA, Node headB) {
    
            Map visitedNode = new HashMap();
            while(headA != null){
        
                if (!visitedNode.containsKey(headA)){
                    visitedNode.put(headA, true);
                }
                else{
                    return headA.data;
                }
        
                headA = headA.next;
            }
    
            while(headB != null){
        
                if (!visitedNode.containsKey(headB)){
                    visitedNode.put(headB, true);
                }
                else{
                    return headB.data;
                }
        
                headB = headB.next;
            }
    
            return -1;
        }
         
         */
        #endregion
    }
}
