﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.DataStructures._01_LinkedLists
{
    class ReverseLinkedList
    {
        #region Java Program

        /*
          Insert Node at the end of a linked list 
          head pointer input could be NULL as well for empty list
          Node is defined as 
          class Node {
             int data;
             Node next;
          }
        */

        // This is a "method-only" submission. 
        // You only need to complete this method. 
        //Node Reverse(Node head)
        //{
        //    if (head == null)
        //        return head;

        //    Node prev = null;
        //    while (head != null)
        //    {
        //        Node next = head.next;
        //        head.next = prev;
        //        prev = head;
        //        head = next;
        //    }
        //    return prev;
        //}

        #endregion
    }
}
