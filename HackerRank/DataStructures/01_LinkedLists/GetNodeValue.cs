﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.DataStructures._01_LinkedLists
{
    class GetNodeValue
    {
        #region Program Description
        /*
         * You’re given the pointer to the head node of a linked list and a specific position. Counting backwards from 
         * the tail node of the linked list, get the value of the node at the given position. A position of 0 
         * corresponds to the tail, 1 corresponds to the node before the tail and so on.
         * 
         * Input Format
         * =============
         * You have to complete the int GetNode(Node* head, int positionFromTail) method which takes two 
         * arguments - the head of the linked list and the position of the node from the tail. positionFromTail 
         * will be at least 0 and less than the number of nodes in the list. You should NOT read any input from 
         * stdin/console.
         * 
         * Constraints 
         * =============
         * Position will be a valid element in linked list.
         * 
         * Output Format
         * =============
         * Find the node at the given position counting backwards from the tail. Then return the data contained in 
         * this node. Do NOT print anything to stdout/console.
         * 
         * Sample Input
         * =============
         * 1 -> 3 -> 5 -> 6 -> NULL, positionFromTail = 0
         * 1 -> 3 -> 5 -> 6 -> NULL, positionFromTail = 2
         * 
         * Sample Output
         * =============
         * 6
         * 3
         * 
         */
        #endregion

        #region Java Program

        /*
          Insert Node at the end of a linked list 
          head pointer input could be NULL as well for empty list
          Node is defined as 
          class Node {
             int data;
             Node next;
          }
        */

        /*
            int GetNode(Node head,int n) {
                 // This is a "method-only" submission. 
                 // You only need to complete this method. 
                int count = 0;
                Node pointer = head;
    
                while(head != null){
                    ++count;
                    head = head.next;
                }
    
                int indexToRetrieve = count - n;
                // Reset count to zero
                count = 1;
    
                while(pointer != null){
                    if(count++ == indexToRetrieve){
                        return pointer.data;
                    }
                    pointer = pointer.next;
                }
    
                return -1;
            }
         
         */

        #endregion
    }
}
