﻿using System;
using System.Collections.Generic;
using HackerRank.DataStructures._08_Common;

namespace HackerRank.DataStructures._01_LinkedLists
{
    static class LinkedListTailInsertNode
    {
        #region Program Description
        /*
         * You’re given the pointer to the head node of a linked list and an integer to add to the list. 
         * Create a new node with the given integer, insert this node at the tail of the linked list and return 
         * the head node. The head pointer given may be null meaning that the initial list is empty.
         * 
         * Input Format
         * ==============
         * You have to complete the Node* Insert(Node* head, int data) method which takes two arguments - the head 
         * of the linked list and the integer to insert. You should NOT read any input from stdin/console.
         * 
         * Output Format
         * ===============
         * Insert the new node at the tail and just return the head of the updated linked list. Do NOT print anything to 
         * stdout/console.
         * 
         * Sample Input
         * ===============
         * NULL, data = 2 
         * 2 --> NULL, data = 3
         * 
         * Sample Output
         * ===============
         * 2 -->NULL
         * 2 --> 3 --> NULL
         * 
         */
        #endregion

        #region Java Code

        /*
          Insert Node at the end of a linked list 
          head pointer input could be NULL as well for empty list
          Node is defined as 
          class Node {
             int data;
             Node next;
          }
        */

        //Node Insert(Node head, int data){
        //    // This is a "method-only" submission. 
        //    // You only need to complete this method.
        //    Node pointer = head;
        //    Node n = new Node();
        //    n.data = data;
        //    //n.next = null;
        //    while (pointer.next != null)
        //    {
        //        pointer = pointer.next;
        //    }

        //    pointer.next = n;
        //    return head;
        //}

        #endregion
    }
}
