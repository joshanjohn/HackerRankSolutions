﻿using System;
using HackerRank.DataStructures._08_Common;

namespace HackerRank.DataStructures._01_LinkedLists
{
    static class PrintLinkedListElements
    {
        #region Program Description
        /*
         * Problem Statement
         * ====================
         * If you’re new to working with linked lists, this is a great exercise to get 
         * familiar with them. You’re given the pointer to the head node of a linked list 
         * and you need to print all its elements in order, one element per line. The 
         * head pointer may be null, i.e., it may be an empty list. In that case, don’t 
         * print anything!
         * 
         * Input Format 
         * ====================
         * You have to complete the void Print(Node* head) method which takes one argument 
         * - the head of the linked list. You should NOT read any input from 
         * stdin/console. There are multiple test cases. For each test case, this method 
         * will be called individually.
         * 
         * Output Format 
         * ====================
         * Print the elements of the linked list to stdout/console (using printf or cout), 
         * one per line.
         * 
         * Sample Input
         * ====================
         * NULL  
         * 1->2->3->NULL
         * 
         * Sample Output
         * ====================
         * 1
         * 2
         * 3
         * 
         */
        #endregion

        private static void xMain()
        {
            var list = new SingleLinkedList();
            for (var i = 1; i <= 10; ++i)
            {
                var newNode = new LinkedListNode {Data = i};
                list.Add(newNode);
            }

            var result = list.Print();

            Console.Write(result);
            Console.ReadKey();
        }

        #region Java Code
        /*
          Print elements of a linked list on console 
          head pointer input could be NULL as well for empty list
          Node is defined as 
          class Node {
             int data;
             Node next;
          }
        */

        //void Print(Node head) {
        //    StringBuilder result = new StringBuilder();
        //    while (head != null){
        //            result.append(head.data);
        //            result.append("\n");
        //            head = head.next;
        //    }
        //    System.out.print(result.toString());
        //}
        #endregion

    }
}
