﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.DataStructures._01_LinkedLists
{
    class InsertNodeIntoSortedDoublyLinkedList
    {
        #region Program Description
        /*
         * You’re given the pointer to the head node of a sorted doubly linked list and an integer to insert into 
         * the list. Create a node and insert it into the appropriate position in the list. The head node might be 
         * NULL to indicate that the list is empty.
         * 
         * Input Format 
         * ==============
         * 
         * You have to complete the Node* SortedInsert(Node* head, int data) method which takes two arguments - the 
         * head of the sorted, doubly linked list and the value to insert. You should NOT read any input from 
         * stdin/console.
         * 
         * Output Format 
         * ==============
         * 
         * Create a node with the given data and insert it into the given list, making sure that the new list is 
         * also sorted. Then return the head node of the updated list. Do NOT print anything to stdout/console.
         * 
         * Sample Input
         * ==============
         * 
         * NULL , data = 2
         * NULL <-- 2 <--> 4 <--> 6 --> NULL , data = 5
         * 
         * Sample Output
         * ==============
         * 
         * NULL <-- 2 --> NULL
         * NULL <-- 2 <--> 4 <--> 5 <--> 6 --> NULL
         * 
         * Explanation 
         * ==============
         * 
         * 1. We have an empty list, 2 is inserted. 
         * 2. Data 5 is inserted such as list remains sorted.
         * 
         */
        #endregion

        #region Java Program
        /*
          Insert Node at the end of a linked list 
          head pointer input could be NULL as well for empty list
          Node is defined as 
          class Node {
             int data;
             Node next;
             Node prev;
          }
        */
        /*
        Node SortedInsert(Node head, int data)
        {

            Node n = new Node();
            n.data = data;

            // base case
            if (head.data == 0 && head.next == null)
            {
                n.prev = head;
                head.next = n;
                return head;
            }

            Node result = head;

            while (head.next != null)
            {
                if (data <= head.data)
                {
                    n.next = head;
                    n.prev = head.prev;
                    head.prev.next = n;
                    break;
                }
                head = head.next;
            }

            if (head.next == null)
            {
                if (data <= head.data)
                {
                    n.next = head;
                    n.prev = head.prev;
                    head.prev.next = n;
                    head.prev = n;
                }
                else
                {
                    n.prev = head;
                    head.next = n;
                }
            }

            return result;
        }
        */
        #endregion
    }
}
