﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.DataStructures._01_LinkedLists
{
    class LinkedListPrintReverse
    {
        #region Problem Statement
        /*
         * 
         * You are given the pointer to the head node of a linked list and you need to print all its elements in 
         * reverse order from tail to head, one element per line. The head pointer may be null meaning that the list 
         * is empty - in that case, do not print anything!
         * 
         * Input Format
         * =============
         * 
         * You have to complete the void ReversePrint(Node* head) method which takes one argument - the head of the 
         * linked list. You should NOT read any input from stdin/console.
         * 
         * Output Format
         * ==============
         * 
         * Print the elements of the linked list in reverse order to stdout/console (using printf or cout) , one per 
         * line.
         * 
         * Sample Input
         * ==============
         * 
         * 1 --> 2 --> NULL
         * 2 --> 1 --> 4 --> 5 --> NULL
         * 
         * Sample Output
         * 
         * 2
         * 1
         * 5
         * 4
         * 1
         * 2
         * 
         * Explanation
         * ==============
         * 
         * 1. First list is printed from tail to head hence 2,1
         * 2. Similarly second list is also printed from tail to head.
         */

        #endregion

        #region Java Program - Mine

        /*
          Print elements of a linked list in reverse order as standard output
          head pointer could be NULL as well for empty list
          Node is defined as 
          struct Node
          {
             int data;
             struct Node *next;
          }
        */

        //void ReversePrint(Node head) {
        //    Node prev = null;
        //    Node curr = head;
    
        //    while(curr != null){
        //        Node next = curr.next;
        //        curr.next = prev;
        //        prev = curr;
        //        curr = next;
        //    }
        //    // Since curr pointer points to null, reference it to the prev node.
        //    curr = prev;
    
        //    while(curr != null){
        //        System.out.println(curr.data);
        //        curr = curr.next;
        //    }
    
        //}

        #endregion

        #region Java Program - Another way efficient

        //void ReversePrint(Node head) {

        //if (head == null)
        //    return;

        //ReversePrint(head.next);

        //System.out.println(head.data);

        //return;
        //}

        #endregion

    }
}
