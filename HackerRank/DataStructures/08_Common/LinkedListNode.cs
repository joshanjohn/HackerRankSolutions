﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.DataStructures._08_Common
{
    internal class LinkedListNode
    {
        internal int Data { get; set; }
        internal LinkedListNode Next { get; set; }
    }
}
