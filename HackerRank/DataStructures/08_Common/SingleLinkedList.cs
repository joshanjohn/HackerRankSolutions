﻿
namespace HackerRank.DataStructures._08_Common
{
    class SingleLinkedList
    {
        //private LinkedListNode _first;
        //private LinkedListNode _last;
        internal int Count { get; set; }
        internal LinkedListNode First { get; private set; }
        internal LinkedListNode Last { get; private set; }

        // Works
        internal void Add(LinkedListNode node)
        {
            if (First == null)
            {
                First = node;
                Last = First;
            }
            else
            {
                Last.Next = node;
                Last = Last.Next;
            }
            ++Count;
        }

        internal void Delete(LinkedListNode node)
        {
            var tmp = First;
            while (tmp != null)
            {
                if (!tmp.Data.Equals(node.Data)) continue;
                tmp = tmp.Next;
                //tmp
                --Count;
            }
        }

        // Works
        internal string Print()
        {
            var result = new System.Text.StringBuilder();
            var head = First;
            while (head != null)
            {
                result.AppendLine(head.Data.ToString());
                head = head.Next;
            }
            return result.ToString();
        }
    }
}
