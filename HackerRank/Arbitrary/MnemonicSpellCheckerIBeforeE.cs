﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Arbitrary
{
    /// <summary>
    /// This spell checker implements the following rule:
    /// "I before E, except after C" is a mnemonic rule of thumb for English spelling.
    /// If one is unsure whether a word is spelled with the sequence ei or ie, the rhyme
    /// suggests that the correct order is ie unless the preceding letter is c, in which case it is ei. 
    /// 
    /// Examples: believe, fierce, collie, die, friend, deceive, ceiling, receipt would be evaulated as spelled correctly
    /// heir, protein, science, seeing, their, and veil would be evaluated as spelled incorrectly.
    /// </summary>
    class MnemonicSpellCheckerIBeforeE
    {
        static void xMain(string[] args)
        {
            Console.WriteLine("Enter a spelling to check: ");
            var word = Console.ReadLine();

            if (Check(word))
                Console.WriteLine(String.Format("The word {0} is RIGHT!", word));
            else
                Console.WriteLine(String.Format("The word {0} is WRRRROOONNNNGGG!", word));

            Console.ReadKey();
        }

        /// <summary>
        /// Returns false if the word contains a letter sequence of "ie" when it is immediately preceded by c
        /// </summary>
        /// <param name="word">The word to be checked</param>
        /// <returns>true when the word is spelled correctly, false otherwise</returns>
        public static bool Check(string word)
        {
            if (word == null) throw new System.ArgumentNullException("Parameter 'word' is not passed");
            if (System.String.IsNullOrEmpty(word.Trim())) return true;
            string lowerCaseWord = word.ToLower();
            const string EI_SEQUENCE = "ei";
            const string IE_SEQUENCE = "ie";

            if (lowerCaseWord.Contains(EI_SEQUENCE))
            {
                // check if the word is preceded by c. 'i' should always come before 'e'. 
                // Except after "c" where it should come after "e".    --- This interpretation of the I before E rule is actually wrong
                if (!isWordPrecededByCharC(lowerCaseWord, EI_SEQUENCE))
                    return false;
            }
            else if (lowerCaseWord.Contains(IE_SEQUENCE))
            {
                if (isWordPrecededByCharC(lowerCaseWord, IE_SEQUENCE))
                    return false;   // As per requirements.
            }

            return true;
        }

        private static bool isWordPrecededByCharC(string lowerCaseWord, string sequence)
        {
            int index = lowerCaseWord.IndexOf(sequence); // This gives first index of ei

            while (index > 0 && index < lowerCaseWord.Length - 1)
            {
                char c = lowerCaseWord[index - 1];
                if (c.Equals('c'))
                    index = lowerCaseWord.IndexOf(sequence, index); // Get next index
                else
                    return false;
            }
            return true;
        }
    }
}
