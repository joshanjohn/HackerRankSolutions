﻿using System;
using HackerRank.Common;

namespace HackerRank.Algorithms
{
    class Staircase
    {
        #region Problem Description
        /*
         * Problem Statement
         * ==================
         * Your teacher has given you the task to draw the structure of a staircase. 
         * Being an expert programmer, you decided to make a program for the same.
         * You are given the height of the staircase. You need to print a staircase as 
         * shown in the example.
         * 
         * Input Format
         * --------------
         * You are given an integer N depicting the height of the staircase.
         * 
         * Constraints 
         * --------------
         * 1 ≤ N ≤ 100
         * 
         * Output Format
         * --------------
         * Draw a staircase of height N in the format given below.
         * 
         * Sample Input
         * --------------
         * 6  
         * 
         * Sample Output
         * --------------
         *      #
         *     ##
         *    ###
         *   ####
         *  #####
         * ######
         * 
         */
        #endregion

        static void xMain(string[] args)
        {
            ushort N;

            string inputHeight = Console.ReadLine();
            if (Utility.isNull(inputHeight))
                return;
            // NOTE:    I am using only return since hacker rank output does not support
            //          output other than the one it has identified

            if (!ushort.TryParse(inputHeight, out N))
                return;

            if (!(0 < N && N < 101))
                return;

            for(int r = 0; r < N; ++r)
            {
                for(int c = 0; c < N; ++c)
                {
                    if (c < (N - 1) - r)
                        Console.Write(" ");
                    else
                        Console.Write("#");
                }
                Console.WriteLine();
            }

            Console.ReadLine();
        }

    }
}
