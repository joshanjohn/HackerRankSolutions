﻿using System;
using HackerRank.Common;

namespace HackerRank.Algorithms._01_Warmup
{
    static class CaesarCipher
    {
        #region Program Description
        /*
         * Julius Caesar protected his confidential information from his enemies by 
         * encrypting it. Caesar rotated every letter in the string by a fixed number K. 
         * This made the string unreadable by the enemy. You are given a string S and the 
         * number K. Encrypt the string and print the encrypted string.
         * 
         * For example: 
         * If the string is middle-Outz and K=2, the encoded string is okffng-Qwvb. 
         * Note that only the letters are encrypted while symbols like - are untouched. 
         * 'm' becomes 'o' when letters are rotated twice, 
         * 'i' becomes 'k', 
         * '-' remains the same because only letters are encoded, 
         * 'z' becomes 'b' when rotated twice.
         * 
         * Input Format
         * --------------
         * Input consists of an integer N equal to the length of the string, 
         * followed by the string S and an integer K.
         * 
         * Constraints 
         * --------------
         * 1 ≤ N ≤ 100 
         * 0 ≤ K ≤ 100 
         * S is a valid ASCII string and doesn't contain any spaces.
         * 
         * Output Format
         * --------------
         * For each test case, print the encoded string.
         * 
         * Sample Input
         * --------------
         * 11
         * middle-Outz
         * 2
         * 
         * Sample Input
         * --------------
         * okffng-Qwvb
         * 
         */
        #endregion

        static void xMain(string[] args)
        {
            string strLength = Console.ReadLine();

            if (Utility.isNull(strLength))
                return;

            int iLength = 0;
            if (!Int32.TryParse(strLength, out iLength))
                return;

            if (iLength < 1 || iLength > 100)
                return; // Constraint 1 handled

            string strMessage = Console.ReadLine();

            if (Utility.isNull(strMessage))
                return;

            if (strMessage.Contains(" "))
                return; // Requirement

            if (!strMessage.Length.Equals(iLength))
                return;

            string strNumberOfRotations = Console.ReadLine();

            if (Utility.isNull(strNumberOfRotations))
                return;

            int iNumberOfRotations = 0;
            if (!Int32.TryParse(strNumberOfRotations, out iNumberOfRotations))
                return;

            if (iNumberOfRotations < 0 || iNumberOfRotations > 100)
                return; // Constraint 1 handled

            Console.WriteLine(RotateString(strMessage, iNumberOfRotations));
            Console.ReadKey();
        }

        static string RotateString(string input, int numberOfRotations)
        {
            char[] arrInput = new char[input.Length];
            int i, j;
            for (i = 0, j = input.Length - 1; i < j; ++i, --j)
            {
                arrInput[i] = RotateCharacter(input[i], numberOfRotations);
                arrInput[j] = RotateCharacter(input[j], numberOfRotations);
            }

            if (i.Equals(j))
            {
                arrInput[i] = RotateCharacter(input[i], numberOfRotations);
            }

            return new String(arrInput);
        }

        static char RotateCharacter(char c, int numberOfRotations)
        {
            int result = (int)c;
            bool process = Char.IsLetter(c);
            if (process)
            {
                //const int ASCII_NUMBER_START = 48;
                //const int ASCII_NUMBER_END = 57;
                const int ASCII_CHAR_UPPER_START = 65;
                const int ASCII_CHAR_UPPER_END = 90;
                const int ASCII_CHAR_LOWER_START = 97;
                const int ASCII_CHAR_LOWER_END = 122;
                int remainingToRotate, overflow;

                //numberOfRotations %= (Char.IsDigit(c) ? 10 : 26);
                //remainingToRotate = (Char.IsDigit(c) ? ASCII_NUMBER_END : ((Char.IsUpper(c) ? ASCII_CHAR_UPPER_END : ASCII_CHAR_LOWER_END))) - result;
                numberOfRotations %= 26;
                remainingToRotate = (Char.IsUpper(c) ? ASCII_CHAR_UPPER_END : ASCII_CHAR_LOWER_END) - result;
                overflow = numberOfRotations - remainingToRotate;
                if (overflow > 0)
                {
                    // The rotation overflows and need to reset rotation from beginning
                    //result = Char.IsDigit(c) ? ASCII_NUMBER_START-1 : ((Char.IsUpper(c) ? ASCII_CHAR_UPPER_START-1 : ASCII_CHAR_LOWER_START-1));
                    result = (Char.IsUpper(c) ? ASCII_CHAR_UPPER_START - 1 : ASCII_CHAR_LOWER_START - 1);
                    numberOfRotations = overflow;
                }

                result += numberOfRotations;
            }
            return (char)result;
        }

        #region Failed Test Cases
        /*
         * 
         * Test Case 1:
         * Input:
         * -------------
         * 10
         * 159357lcfd
         * 98
         * Output:
         * -------------
         * 159357fwzx
         * 
         * SIMPLE REASON:   I rotated the numbers too. ONLY LETTERS are rotated.
         * LESSONS LEARNT:          
         * 1 - READ REQUIREMENTS BEFORE SUBMITTING.
         * 2 - DON'T DELAY SUBMISSION OF AN EASY TASK. I GET BORED FAST.
         * 
         */
        #endregion
    }
}