﻿using System;
using HackerRank.Common;

namespace HackerRank.Algorithms
{
    class LibraryFine
    {
        #region Problem Description
        /*
         * Problem Statement
         * ==================
         * The Head Librarian at a library wants you to make a program that calculates 
         * the fine for returning the book after the return date. You are given the actual 
         * and the expected return dates. Calculate the fine as follows:
         * 
         * 1) If the book is returned on or before the expected return date, no fine will be 
         *    charged, in other words fine is 0.
         * 2) If the book is returned in the same month as the expected return date, 
         *    Fine = 15 Hackos × Number of late days
         * 3) If the book is not returned in the same month but in the same year as the 
         *    expected return date, Fine = 500 Hackos × Number of late months
         * 4) If the book is not returned in the same year, the fine is fixed at 10000 Hackos.
         * 
         * Input Format
         * --------------
         * You are given the actual and the expected return dates in D M Y format respectively. 
         * There are two lines of input. The first line contains the D M Y values for the 
         * actual return date and the next line contains the D M Y values for the expected 
         * return date.
         * 
         * Constaints:
         * --------------
         * 1 ≤ D ≤ 31 
         * 1 ≤ M ≤ 12 
         * 1 ≤ Y ≤ 3000
         * 
         * Output Format
         * --------------
         * Output a single value equal to the fine.
         * 
         */
        #endregion

        static void xMain(string[] args)
        {
            #region Parse and Validate Actual Date

            string actual = Console.ReadLine();

            if (Utility.isNull(actual))
                return;

            string[] arrActual = actual.Split(' ');
            if (arrActual.Length != 3)
                return;

            ushort actualDay, actualMonth, actualYear;
            if (!ushort.TryParse(arrActual[0], out actualDay))
                return;
            else if (!ValidDay(actualDay))
                return;

            if (!ushort.TryParse(arrActual[1], out actualMonth))
                return;
            else if (!ValidMonth(actualMonth))
                return;

            if (!ushort.TryParse(arrActual[2], out actualYear))
                return;
            else if (!ValidYear(actualYear))
                return;

            #endregion

            #region Parse and Validate Expected Date

            string expected = Console.ReadLine();

            if (Utility.isNull(expected))
                return;

            string[] arrExpected = expected.Split(' ');
            if (arrExpected.Length != 3)
                return;

            ushort expDay, expMonth, expYear;
            if (!ushort.TryParse(arrExpected[0], out expDay))
                return;
            else if (!ValidDay(expDay))
                return;

            if (!ushort.TryParse(arrExpected[1], out expMonth))
                return;
            else if (!ValidMonth(expMonth))
                return;

            if (!ushort.TryParse(arrExpected[2], out expYear))
                return;
            else if (!ValidYear(expYear))
                return;

            #endregion
            
            DateTime dtActual, dtExpected;

            if (!DateTime.TryParse(String.Format("{0}/{1}/{2}", actualMonth, actualDay, actualYear), out dtActual) || !DateTime.TryParse(String.Format("{0}/{1}/{2}", expMonth, expDay, expYear), out dtExpected))
                return;

            int compareResult = DateTime.Compare(dtActual, dtExpected);

            if (compareResult <= 0)
            {
                Console.WriteLine(0);       // Fine rule #1
                //return;
            }
            else
            {
                if (dtActual.Year > dtExpected.Year)
                {
                    Console.WriteLine(10000);   // Fine rule #4
                    //return;
                }
                else if (dtActual.Month > dtExpected.Month)
                {
                    // Same year but greater than expected
                    int fine = 500 * (dtActual.Month - dtExpected.Month);
                    Console.WriteLine(fine);    // Fine rule #3
                    // return;
                }
                else
                {
                    // Same month and Same Year
                    int fine = 15 * (dtActual.Day - dtExpected.Day);
                    Console.WriteLine(fine);    // Fine rule #2
                    // return;
                }
            }

            Console.ReadLine();
        }

        private static bool ValidYear(ushort year)
        {
            if (year < 1 || year > 3000)
                return false;
            else
                return true;
        }

        private static bool ValidMonth(ushort month)
        {
            if (month < 1 || month > 12)
                return false;
            else
                return true;
        }

        private static bool ValidDay(ushort day)
        {
            if (day < 1 || day > 31)
                return false;
            else
                return true;
        }

    }
}
