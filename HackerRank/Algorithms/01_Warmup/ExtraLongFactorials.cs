﻿using System;
using System.Numerics;
using HackerRank.Common;

namespace HackerRank.Algorithms
{
    class ExtraLongFactorials
    {
        #region Program Description
        /* 
         * You are given an integer N. Print the factorial of this number.
         * 
         *              N! = N × (N−1) × (N−2) × ⋯ × 3 × 2 × 1
         *              
         * Note: Factorials of N > 20 can't be stored even in a 64−bit long long 
         * variable. Big integers must be used for such calculations. Languages 
         * like Java, Python, Ruby etc. can handle big integers but we need to write 
         * additional code in C/C++ to handle such large values.
         * 
         * We recommend solving this challenge using BigIntegers.
         * 
         * Input Format 
         * -------------
         * Input consists of a single integer N.
         * 
         * Constraints
         * 1 ≤ N ≤ 100
         * 
         * Output Format 
         * -------------
         * Output the factorial of N.
         * 
         * Sample Input
         * -------------
         * 25
         * 
         * Sample Output
         * -------------
         * 15511210043330985984000000
         * 
         */
        #endregion

        static void xMain(string[] args)
        {
            var input = Console.ReadLine();
            if (Utility.isNull(input))
                return;

            ushort inputNum = 0;
            if (!ushort.TryParse(input, out inputNum))
                return;
            
            if (inputNum < 1 || inputNum > 100)
                return;

            Console.WriteLine(facto(inputNum));
            Console.ReadKey();
        }

        static BigInteger facto(BigInteger num)
        {
            if (num < 0)
                return 0;
            else if (num.Equals(0))
                return 1;
            else
                return num * facto(num - 1);
        }

    }
}
