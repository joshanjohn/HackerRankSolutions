﻿using HackerRank.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Algorithms
{
    class DiagonalDifference
    {
        static void xMain(string[] args)
        {
            // input
            /*
             * N = NxN Matrix
             * N lines of N numbers
             * 
             */
            short n, sum;
            var matrixN = Console.ReadLine();
            if (Utility.isNull(matrixN))
                return;
            if (!Int16.TryParse(matrixN, out n))
                return;
            sum = CalculateMatrixSum(n);
            Console.WriteLine(sum);
        }
        private static short CalculateMatrixSum(short n)
        {
            short result, diag1 = 0, diag2 = 0;
            for (int i = 0; i < n; i++)
            {
                string line = Console.ReadLine();
                if (null == line)
                    return 0;
                string[] arrNums = line.Split(' ');
                for (int j = 0; j < n; j++)
                {
                    short num;
                    if (!Int16.TryParse(arrNums[j], out num))
                        return 0;
                    if (i.Equals(j))
                        diag1 += num;
                    if (j.Equals(n - 1 - i))
                        diag2 += num;
                }
            }
            result = (short)(diag1 - diag2);
            return Math.Abs(result);
        }
    }
}
