﻿using HackerRank.Common;
using System;

namespace HackerRank.Algorithms
{
    class TimeConversion
    {
        #region Problem Description
        /*
         * Problem Statement
         * ==================
         * You are given time in AM/PM format. Convert this into a 24 hour format.
         * Note: Midnight is 12:00:00AM or 00:00:00 and 12 Noon is 12:00:00.
         * 
         * Input Format
         * --------------
         * Input consists of time in the AM/PM format i.e. hh:mm:ssAM or hh:mm:ssPM 
         * where 
         * 01 ≤ hh ≤ 12 
         * 00 ≤ mm ≤ 59 
         * 00 ≤ ss ≤ 59
         * 
         * Output Format
         * --------------
         * You need to print the time in 24 hour format i.e. hh:mm:ss 
         * where 
         * 00 ≤ hh ≤ 23 
         * 00 ≤ mm ≤ 59 
         * 00 ≤ ss ≤ 59
         * 
         */
        #endregion

        static void xMain(string[] args)
        {
            string time = Console.ReadLine();
            if (Utility.isNull(time))
                return;
            // NOTE:    I am using only return since hacker rank output does not support
            //          output other than the one it has identified

            string[] arr = time.Split(':');
            if (arr.Length != 3)
                return;

            ushort hour, min, sec;

            if (!ushort.TryParse(arr[0], out hour))
                return;
            else if (hour > 12 || hour < 1)
                return;

            if (!ushort.TryParse(arr[1], out min))
                return;
            else if (min > 59 || min < 0)  // dont' have to search for <0 since this is ushort. the TryParse will fail
                return;

            if (!ushort.TryParse(arr[2].Substring(0, 2), out sec))
                return;
            else if (sec > 59 || min < 0)  // dont' have to search for <0 since this is ushort. the TryParse will fail
                return;

            if (arr[2].Substring(2, 2).Contains("AM"))
            {
                if (hour.Equals(12))
                {
                    arr[0] = "00";
                }
                arr[2] = arr[2].Substring(0, 2);
            }
            else if (arr[2].Substring(2, 2).Contains("PM"))
            {
                if (hour < 12)
                {
                    arr[0] = (hour + 12).ToString();
                }
                arr[2] = arr[2].Substring(0, 2);
            }
            else
                return;

            Console.WriteLine(string.Format("{0}:{1}:{2}", arr[0], arr[1], arr[2]));
            Console.ReadLine();

        }

    }
}
