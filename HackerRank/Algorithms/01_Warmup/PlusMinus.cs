﻿using HackerRank.Common;
using System;

namespace HackerRank.Algorithms
{
    class PlusMinus
    {
        #region Problem Description
        /*
         * Problem Statement
         * ==================
         * You're given an array containing integer values. You need to print the fraction 
         * of count of positive numbers, negative numbers and zeroes to the total numbers. 
         * Print the value of the fractions correct to 3 decimal places.
         * 
         * Input Format
         * --------------
         * First line contains N, which is the size of the array. 
         * Next line contains N integers A1,A2,A3,⋯,AN, separated by space.
         * 
         * Constraints 
         * --------------
         * 1≤N≤100 
         * −100≤Ai≤100
         * 
         * Output Format
         * --------------
         * Output three values on different lines equal to the fraction of count of positive 
         * numbers, negative numbers and zeroes to the total numbers respectively correct to 
         * 3 decimal places.
         * 
         * Sample Input
         * --------------
         * 6
         * -4 3 -9 0 4 1   
         * 
         * Sample Output
         * --------------
         * 0.500
         * 0.333
         * 0.167
         * 
         */
        #endregion
        
        static void xMain(string[] args)
        {
            ushort N;

            string arrLength = Console.ReadLine();
            if (Utility.isNull(arrLength))
                return; 
            // NOTE:    I am using only return since hacker rank output does not support
            //          output other than the one it has identified

            if (!ushort.TryParse(arrLength, out N))
                return;

            if (!(0 < N && N < 101))
                return;

            string arrInput = Console.ReadLine();
            if (arrInput.Trim().Length.Equals(0))
                return;
            string[] arr = arrInput.Split(' ');

            if (arr.Length != N)
                return;

            short ptiveCount = 0, ntiveCount = 0, zCount = 0, currNumber;
            double ptiveFraction = 0.0f, ntiveFraction = 0.0f, zFraction = 0.0f;

            for (int i = 0; i < arr.Length; ++i)
            {
                if (!short.TryParse(arr[i], out currNumber))
                    return;

                if (currNumber < 0)
                    ++ntiveCount;
                else if (currNumber > 0)
                    ++ptiveCount;
                else
                    ++zCount;
            }

            ptiveFraction = Math.Round((ptiveCount / (double)N), 3);
            ntiveFraction = Math.Round((ntiveCount / (double)N), 3);
            zFraction = Math.Round((zCount / (double)N), 3);

            Console.WriteLine(ptiveFraction);
            Console.WriteLine(ntiveFraction);
            Console.WriteLine(zFraction);

            Console.ReadLine();// Needed to see output in the console
        }

    }
}
