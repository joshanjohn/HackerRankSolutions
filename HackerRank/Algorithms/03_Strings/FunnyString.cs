﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Algorithms._03_Strings
{
    class FunnyString
    {
        static void xMain(String[] args)
        {
            while (true)
            {
                UInt16 uINumberOfStrings = 0;
                UInt16 stringCounter = 0;
                List<string> lstInputStrings;

                #region Constraint 1

                var numberInput = Console.ReadLine();
                if (null != numberInput)
                {
                    if (!UInt16.TryParse(numberInput, out uINumberOfStrings))
                    {
                        Console.WriteLine("Please enter integer value");
                        continue;
                    }
                }
                else
                {
                    Console.WriteLine("Please enter integer value");
                    continue;
                }

                if (1 > uINumberOfStrings || uINumberOfStrings > 10)
                {
                    Console.WriteLine("Please enter a value between 1 and 10 inclusive");
                    continue;
                }

                #endregion

                #region Contraint 2

                lstInputStrings = new List<string>();
                while (stringCounter < uINumberOfStrings)
                {
                    var str = Console.ReadLine();
                    if (null == str)
                    {
                        continue;
                    }

                    if (str.Length < 2 || str.Length > 10000)
                    {
                        Console.WriteLine("Please enter a string with length greater than 2 and less than 10,000");
                        continue;
                    }

                    lstInputStrings.Add(str);
                    ++stringCounter;
                }

                foreach (string str in lstInputStrings)
                {
                    if (isFunny(str))
                        Console.WriteLine("Funny");
                    else
                        Console.WriteLine("Not Funny");
                }

                break;

                #endregion
            }
        }

        static bool isFunny(string str)
        {
            int i;
            for (i = 1; i < str.Length; ++i)
            {
                int j = str.Length - 1 - i;
                if (Math.Abs(str[i] - str[i - 1]) != Math.Abs(str[j] - str[j + 1]))
                    break;
            }

            if (i.Equals(str.Length))
                return true;
            else
                return false;
        }
    }
}
