﻿using HackerRank.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Algorithms._02_Implementation
{
    class AngryProfessor
    {
        #region Program Description
        /*
         * The professor is conducting a course on Discrete Mathematics to a class of N students. 
         * He is angry at the lack of their discipline, and he decides to cancel the class if there 
         * are fewer than K students present after the class starts.
         * 
         * Given the arrival time of each student, your task is to find out if the class gets 
         * cancelled or not.
         * 
         * Input Format
         * -------------
         * The first line of the input contains T, the number of test cases. Each test case contains
         * two lines.
         * The first line of each test case contains two space-separated integers, N and K.
         * The next line contains N space-separated integers, a1,a2,…,aN, representing the arrival 
         * time of each student.
         * 
         * If the arrival time of a given student is a non-positive integer (ai ≤ 0), then the 
         * student enters before the class starts. If the arrival time of a given student is a 
         * positive integer (ai>0), the student enters after the class has started.
         * 
         * Output Format
         * -------------
         * For each testcase, print "YES" (without quotes) if the class gets cancelled and "NO" 
         * (without quotes) otherwise.
         * 
         * Constraints
         * ------------
         * 1 ≤ T ≤ 10
         * 1 ≤ N ≤ 1000
         * 1 ≤ K ≤ N
         * −100 ≤ ai ≤ 100, where i ∈ [1,N]
         * 
         * Note
         * -----
         * If a student enters the class exactly when it starts (ai=0), the student is considered 
         * to have entered before the class has started.
         * 
         * Sample Input
         * -------------
         * 2
         * 4 3
         * -1 -3 4 2
         * 4 2
         * 0 -1 2 1
         * 
         * Sample Output
         * -------------
         * YES
         * NO
         * 
         * Explanation
         * -------------
         * 
         * For the first test case, K=3, i.e., the professor wants at least 3 students to be in 
         * class but there are only 2 who have arrived on time (−3 and −1), hence the class gets 
         * cancelled.
         * For the second test case, K=2, i.e, the professor wants at least 2 students to be in 
         * class and there are 2 who have arrived on time (0 and −1), hence the class does not get 
         * cancelled.
         * 
         */
        #endregion

        static void xMain()
        {
            string input;
            if (!Utility.ReadFromConsole(out input))
                return;

            ushort tCases;
            if (!ushort.TryParse(input, out tCases))
                return;

            while (tCases > 0)
            {
                string N_and_K;
                if (!Utility.ReadFromConsole(out N_and_K))
                    return;
                
                string[] arrNK = N_and_K.Split(' ');
                if (arrNK.Length != 2)
                    return;

                ushort N;
                ushort K;

                if (!ushort.TryParse(arrNK[0], out N))
                    return;

                if (N < 1 || N > 1000)
                    return;

                if (!ushort.TryParse(arrNK[1], out K))
                    return;

                if (K < 1 || K > N)
                    return;

                string listOfTimes;
                if (!Utility.ReadFromConsole(out listOfTimes))
                    return;

                string[] arrTimes = listOfTimes.Split(' ');
                if (arrTimes.Length != N)
                    return;

                ushort onTimeCount = 0;
                foreach (string str in arrTimes)
                {
                    short ai;
                    if (!short.TryParse(str, out ai))
                        return;

                    if (ai < -100 || ai > 100)
                        return;

                    if (ai <= 0)
                        ++onTimeCount;
                }

                if (onTimeCount >= K)
                    Console.WriteLine("NO");
                else
                    Console.WriteLine("YES");
                
                --tCases;
            }

            Console.ReadKey();
        }
    }
}
