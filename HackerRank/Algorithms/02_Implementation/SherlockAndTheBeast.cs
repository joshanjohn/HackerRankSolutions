﻿using HackerRank.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace HackerRank.Algorithms._02_Implementation
{
    class SherlockAndTheBeast
    {
        // TOUGH PROBLEM //

        #region Program Description
        /*
         * Sherlock Holmes is getting paranoid about Professor Moriarty, his arch-enemy. 
         * All his efforts to subdue Moriarty have been in vain. These days Sherlock is 
         * working on a problem with Dr. Watson. Watson mentioned that the CIA has been 
         * facing weird problems with their supercomputer, 'The Beast', recently.
         * 
         * This afternoon, Sherlock received a note from Moriarty, saying that he has 
         * infected 'The Beast' with a virus. Moreover, the note had the number N printed on 
         * it. After doing some calculations, Sherlock figured out that the key to remove 
         * the virus is the largest Decent Number having N digits.
         * 
         * A Decent Number has the following properties:
         * 1) 3, 5, or both as its digits. No other digit is allowed.
         * 2) Number of times 3 appears is divisible by 5.
         * 3) Number of times 5 appears is divisible by 3.
         * 
         * Meanwhile, the counter to the destruction of 'The Beast' is running very fast. 
         * Can you save 'The Beast', and find the key before Sherlock?
         * 
         * Input Format
         * -------------
         * The 1st line will contain an integer T, the number of test cases. This is 
         * followed by T lines, each containing an integer N. i.e. the number of digits in 
         * the number.
         * 
         * Output Format
         * -------------
         * 
         * Largest Decent Number having N digits. If no such number exists, tell Sherlock 
         * that he is wrong and print −1.
         * 
         * Constraints
         * -------------
         * 1 ≤ T ≤ 20
         * 1 ≤ N ≤ 100000
         * 
         * Sample Input
         * -------------
         * 4
         * 1
         * 3
         * 5
         * 11
         * 
         * Sample Output
         * -------------
         * -1
         * 555
         * 33333
         * 55555533333
         * 
         * Explanation
         * -------------
         * For N=1, there is no such number.
         * For N=3, 555 is the only possible number.
         * For N=5, 33333 is the only possible number.
         * For N=11, 55555533333 and all permutations of these digits are valid numbers; 
         * among them, the given number is the largest one.
         * 
         */
        #endregion

        static void xMain()
        {
            string testcases;
            if (!Utility.ReadFromConsole(out testcases))
                return;

            ushort casesCount;
            if (!ushort.TryParse(testcases, out casesCount))
                return;

            if (casesCount < 1 || casesCount > 20)
                return;

            while (casesCount > 0)
            {
                string number;
                if (!Utility.ReadFromConsole(out number))
                    return;

                int N;
                if (!int.TryParse(number, out N))
                    return;

                if (N < 1 || N > 100000)
                    return;

                BigInteger numberOfIterations = 0;
                string fives = "555";
                string threes = "33333";
                StringBuilder sbResult = new StringBuilder();

                if (N < 3)
                    Console.WriteLine("-1");
                else if (N % 3 == 0)
                {
                    numberOfIterations = N / 3;
                    for (int i = 0; i < numberOfIterations; ++i)
                        sbResult.Append(fives);
                    Console.WriteLine(sbResult.ToString());
                }
                else if (N % 3 == 1 && N > 7)
                {
                    numberOfIterations = (N - 10) / 3;  // Minus 5 two times
                    for (int i = 0; i < numberOfIterations; ++i)
                        sbResult.Append(fives);
                    sbResult.Append(threes);
                    sbResult.Append(threes);
                    Console.WriteLine(sbResult.ToString());
                }
                else if (N % 3 == 2 && N > 7)
                {
                    numberOfIterations = (N - 5) / 3;   // Minus 5 once to make it divisible by 3
                    for (int i = 0; i < numberOfIterations; ++i)
                        sbResult.Append(fives);
                    sbResult.Append(threes);
                    Console.WriteLine(sbResult.ToString());
                }
                else if (N % 5 == 0)
                {
                    numberOfIterations = N / 5;
                    for (int i = 0; i < numberOfIterations; ++i)
                        sbResult.Append(threes);
                    Console.WriteLine(sbResult.ToString());
                }
                else
                    Console.WriteLine("-1");

                --casesCount;
            }
            Console.ReadKey();
        }

        #region Test Case Errors

        /*
         TC 1:
         ==========
         Input:
            10
            1
            2
            3
            4
            5
            6
            7
            8
            9
            10
         
         Output:
          
           -1
           -1
           555
           -1
           33333
           555555
           -1
           55533333
           555555555
           3333333333
         * 
         * TC 2:
         * ==========
         * 
         * Input:
         *  20
            26786
            28643
            62205
            99775
            42471
            51463
            4296
            80994
            57669
            72134
            25907
            19949
            72832
            99631
            90974
            38777
            59798
            15508
            57254
            60757
         * 
         * Output:
         * HUGE O_O
         * 
         * 
         * Input:
         *  5
            2194
            12002
            21965
            55140
            57634
         * 
         * Output:
         * not -1
         * not -1
         * not -1
         * not -1
         * not -1
         * 
         */

        #endregion

        #region LOGIC

        /*
         * if (num % 3 == 0) - Divisible by 3
         * if (num % 3 == 1) - Minus by 5 twice and then will be divisible by 3
         * if (num % 3 == 2) - Minus by 5 once and then will be divisible by 3
         * 
         */

        #endregion
    }
}
