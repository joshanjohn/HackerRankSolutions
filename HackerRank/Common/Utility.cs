﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HackerRank.Common
{
    internal static class Utility
    {
        internal static bool isNull(this Object obj)
        {
            if (null == obj)
                return true;
            else
                return false;
        }

        internal static bool ReadFromConsole(out string input)
        {
            input = Console.ReadLine();
            if (Utility.isNull(input))
                return false;

            return true;
        }
    }
}
